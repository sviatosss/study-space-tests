package com.sviatosss.testingapp.event;

import com.sviatosss.testingapp.model.Tag;
import lombok.Getter;
import org.springframework.context.ApplicationEvent;

import java.util.List;

@Getter
@SuppressWarnings("serial")
public class OnNewTestWrapEvent extends ApplicationEvent {

    private String id;
    private List<Tag> tags;

    public OnNewTestWrapEvent(String id, List<Tag> tags) {
        super(id);
        this.id = id;
        this.tags = tags;
    }
}
