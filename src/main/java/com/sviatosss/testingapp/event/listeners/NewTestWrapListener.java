package com.sviatosss.testingapp.event.listeners;

import com.sviatosss.testingapp.event.OnNewTestWrapEvent;
import com.sviatosss.testingapp.model.Tag;
import com.sviatosss.testingapp.repository.TagRepository;
import com.sviatosss.testingapp.repository.TestWrapRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.UUID;

@Component
public class NewTestWrapListener implements ApplicationListener<OnNewTestWrapEvent> {

    @Autowired
    private TagRepository tagRepository;

    @Autowired
    private TestWrapRepository testWrapRepository;

    // API

    @Override
    public void onApplicationEvent(final OnNewTestWrapEvent event) {
        this.confirmRegistration(event);
    }

    private void confirmRegistration(final OnNewTestWrapEvent event) {
        final String id = event.getId();
        List<Tag> tags = event.getTags();

        tags = tagRepository.saveAll(tags);
        System.out.println("add tags");
        testWrapRepository.addTags(id, tags);
        System.out.println("add tags to testWrap");
    }
}
