package com.sviatosss.testingapp.repository;

import com.sviatosss.testingapp.model.Tag;
import com.sviatosss.testingapp.model.Test;
import com.sviatosss.testingapp.model.TestWrap;
import com.sviatosss.testingapp.model.enums.ESubject;
import com.sviatosss.testingapp.model.сhallenge.Challenge;
import com.sviatosss.testingapp.model.сhallenge.ChallengeUser;
import com.sviatosss.testingapp.model.сhallenge.EStatusChallenge;
import com.sviatosss.testingapp.model.сhallenge.ETypeChallengeUser;
import com.sviatosss.testingapp.service.SequenceGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ChallengeRepository {
    @Autowired
    private MongoTemplate mongoTemplate;

    public Challenge createChallenge(Challenge challenge) {
        return mongoTemplate.save(challenge, "challenges");
    }

    public Challenge getChallengeById(String challengeId) {
        return mongoTemplate.findOne( Query.query(Criteria.where("id").is(challengeId)), Challenge.class);
    }


    public Challenge joinToChallenge(String challengeId, ChallengeUser challengeUser){
        Challenge challenge = mongoTemplate.findOne( Query.query(Criteria.where("id").is(challengeId)), Challenge.class);
        //Challenge with this id not found
        assert challenge != null;

        //Challenge max users check
        if (challenge.getMaxUsers() == challenge.getUsers().size()) return null;

        //Challenge username unique check
        for (ChallengeUser user:challenge.getUsers()) {
            if (user.getUsername().equals(challengeUser.getUsername())) return null;
        }

        // ??? check if not started ???

        // if change was with status waiting set to Ready (as user is connected)
        if (challenge.getStatus() == EStatusChallenge.CREATED_WAITING) challenge.setStatus(EStatusChallenge.CREATED_READY);

        //add new user
        challenge.addChallengeUser(challengeUser);

        // update and add new user
        return mongoTemplate.save(challenge, "challenges");
    }

    public void removeChallenge(String challengeId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("id").is(challengeId));
        mongoTemplate.remove(query, Challenge.class);
    }

    public Challenge startChallenge(String challengeId) {
        Challenge challenge = mongoTemplate.findOne( Query.query(Criteria.where("id").is(challengeId)), Challenge.class);
        //Challenge with this id not found
        assert challenge != null;

        // set status of challenge to STARTED
        challenge.setStatus(EStatusChallenge.STARTED);

        return mongoTemplate.save(challenge, "challenges");
    }

    public Challenge addPoint(String challengeId, ChallengeUser challengeUser){
        Challenge challenge = mongoTemplate.findOne( Query.query(Criteria.where("id").is(challengeId)), Challenge.class);
        //Challenge with this id not found
        assert challenge != null;

        //Challenge username unique check
        for (ChallengeUser user:challenge.getUsers()) {
            if (user.getUsername().equals(challengeUser.getUsername())) user.setPoints(challengeUser.getPoints());
        }

        // update and set new point to user
        return mongoTemplate.save(challenge, "challenges");
    }

    public Challenge doneChallenge(String challengeId) {
        Challenge challenge = mongoTemplate.findOne( Query.query(Criteria.where("id").is(challengeId)), Challenge.class);
        //Challenge with this id not found
        assert challenge != null;

        // set status of challenge to DONE
        challenge.setStatus(EStatusChallenge.DONE);

        return mongoTemplate.save(challenge, "challenges");
    }
}
