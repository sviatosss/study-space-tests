package com.sviatosss.testingapp.repository;

import com.sviatosss.testingapp.model.Tag;
import com.sviatosss.testingapp.model.Test;
import com.sviatosss.testingapp.model.TestWrap;
import com.sviatosss.testingapp.model.enums.ESubject;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class TagRepository {
    @Autowired
    private MongoTemplate mongoTemplate;

    public Tag save(Tag tag) {
        return mongoTemplate.save(tag);
    }

    public List<Tag> saveAll(List<Tag> tags) {
        return (List<Tag>) mongoTemplate.insert(tags, Tag.class);
    }

    public Tag getTagById(String id){
        return mongoTemplate.findOne(Query.query(Criteria.where("_id").is(id)), Tag.class);
    }

    public Tag getTagByTitle(String title){
        return mongoTemplate.findOne(Query.query(Criteria.where("title").is(title)), Tag.class);
    }

    public List<Tag> getTagsByESubject(ESubject eSubject){
        return mongoTemplate.find(Query.query(Criteria.where("subject").is(eSubject)), Tag.class);
    }

    public List<Tag> getTagsBySubjectAndCharsOfTitle(ESubject subject, String chars){
        Query query = new Query();
        query.addCriteria(Criteria.where("subject").is(subject));
        query.addCriteria(Criteria.where("title").regex("^"+ chars));
        query.limit(5);
        return mongoTemplate.find(query, Tag.class);
    }

    public void deleteById(String id) {
        Query query = new Query();
        query.addCriteria(Criteria.where("id").is(id));
        mongoTemplate.remove(query, Tag.class);
    }


    //    public TestWrap update(TestWrap TestWrap){
//        Query query = new Query();
//        query.addCriteria(Criteria.where("id").is(TestWrap.getId()));
//        Update update = new Update();
//        update.set("name", TestWrap.getName());
//        update.set("description", TestWrap.getDescription());
//        return mongoTemplate.findAndModify(query, update, TestWrap.class);
//    }
//
//    public List findTestWrapByName(String deptName){
//        Query query = new Query();
//        query.addCriteria(Criteria.where("name").is(deptName));
//        return mongoTemplate.find(query, TestWrap.class);
//    }
//
}
