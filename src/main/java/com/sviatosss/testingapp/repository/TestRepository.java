package com.sviatosss.testingapp.repository;

import com.sviatosss.testingapp.model.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class TestRepository {
    @Autowired
    private MongoTemplate mongoTemplate;

    public List findAll() {
        return mongoTemplate.findAll(Test.class);
    }

    public Test save(Test Test) {
        mongoTemplate.save(Test);
        return Test;
    }

//    public Test update(Test Test){
//        Query query = new Query();
//        query.addCriteria(Criteria.where("id").is(Test.getId()));
//        Update update = new Update();
//        update.set("name", Test.getName());
//        update.set("description", Test.getDescription());
//        return mongoTemplate.findAndModify(query, update, Test.class);
//    }
//
//    public List findTestByName(String deptName){
//        Query query = new Query();
//        query.addCriteria(Criteria.where("name").is(deptName));
//        return mongoTemplate.find(query, Test.class);
//    }
//
//    public void deleteById(String deptId) {
//        Query query = new Query();
//        query.addCriteria(Criteria.where("id").is(deptId));
//        mongoTemplate.remove(query, Test.class);
//    }
}
