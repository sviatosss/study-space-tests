package com.sviatosss.testingapp.repository;

import com.sviatosss.testingapp.model.Tag;
import com.sviatosss.testingapp.model.Test;
import com.sviatosss.testingapp.model.TestWrap;
import com.sviatosss.testingapp.model.enums.ESubject;
import com.sviatosss.testingapp.service.SequenceGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class TestWrapRepository {
    @Autowired
    SequenceGenerator sequenceGenerator;

    @Autowired
    private MongoTemplate mongoTemplate;

    public List findAll() {
        return mongoTemplate.findAll(TestWrap.class);
    }

    public TestWrap save(TestWrap testWrap) {
        for (Test test: testWrap.getTests()) {
            test.setId(sequenceGenerator.generateSequence(Test.SEQUENCE__NAME));
        }
        return mongoTemplate.save(testWrap);
    }

    public TestWrap addTest(String id, Test test){
        TestWrap testWrap = mongoTemplate.findOne( Query.query(Criteria.where("_id").is(id)), TestWrap.class);
        assert testWrap != null;
        test.setId(sequenceGenerator.generateSequence(Test.SEQUENCE__NAME));
        testWrap.addTest(test);
        return mongoTemplate.save(testWrap, "test_wraps");
    }

    public TestWrap getTestWrapById(String id){
        return mongoTemplate.findOne(Query.query(Criteria.where("_id").is(id)), TestWrap.class, "test_wraps");
    }

    public List<TestWrap> getTestWrapByESubject(ESubject eSubject){
        return mongoTemplate.find(Query.query(Criteria.where("subject").is(eSubject)), TestWrap.class, "test_wraps");
    }

    public List<TestWrap> getTestWrapByUserId(Long userId){
        return mongoTemplate.find(Query.query(Criteria.where("userId").is(userId)), TestWrap.class, "test_wraps");
    }

    public void addTags(String id, List<Tag> tags){
        TestWrap testWrap = mongoTemplate.findOne(Query.query(Criteria.where("_id").is(id)), TestWrap.class);
        testWrap.addTags(tags);
        mongoTemplate.save(testWrap, "test_wraps");
    }

    public void deleteById(String id) {
        Query query = new Query();
        query.addCriteria(Criteria.where("id").is(id));
        mongoTemplate.remove(query, TestWrap.class);
    }

    //    public TestWrap update(TestWrap TestWrap){
//        Query query = new Query();
//        query.addCriteria(Criteria.where("id").is(TestWrap.getId()));
//        Update update = new Update();
//        update.set("name", TestWrap.getName());
//        update.set("description", TestWrap.getDescription());
//        return mongoTemplate.findAndModify(query, update, TestWrap.class);
//    }
//
//    public List findTestWrapByName(String deptName){
//        Query query = new Query();
//        query.addCriteria(Criteria.where("name").is(deptName));
//        return mongoTemplate.find(query, TestWrap.class);
//    }
//
}
