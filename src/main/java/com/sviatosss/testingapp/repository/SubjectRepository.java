package com.sviatosss.testingapp.repository;

import com.sviatosss.testingapp.model.Subject;
import com.sviatosss.testingapp.model.Tag;
import com.sviatosss.testingapp.model.Test;
import com.sviatosss.testingapp.model.enums.ESubject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class SubjectRepository {
    @Autowired
    private MongoTemplate mongoTemplate;

    public List<Subject> findAll() {
        return mongoTemplate.findAll(Subject.class);
    }

    public Subject save(Subject subject) {
        mongoTemplate.save(subject);
        return subject;
    }
}
