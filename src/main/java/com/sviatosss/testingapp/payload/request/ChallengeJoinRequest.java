package com.sviatosss.testingapp.payload.request;

import com.sviatosss.testingapp.model.сhallenge.ChallengeUser;
import com.sviatosss.testingapp.model.сhallenge.ETypeChallengeUser;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ChallengeJoinRequest {
    @NotBlank
    private String challengeId;

    private Long userId;

    @NotBlank
    private String username;

    public ChallengeUser convert(){
        ChallengeUser challengeUser = new ChallengeUser();
        challengeUser.setUsername(this.username);
        challengeUser.setId(this.userId);
        //ChallengeUser type set
        if (challengeUser.getId() == null){
            challengeUser.setType(ETypeChallengeUser.UNAUTHENTICATED);
        }else challengeUser.setType(ETypeChallengeUser.AUTHENTICATED);

        return challengeUser;
    }
}
