package com.sviatosss.testingapp.payload.request;

import com.sviatosss.testingapp.model.сhallenge.ChallengeUser;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ChallengeAddPointRequest {
    @NotBlank
    private String challengeId;
    @NotBlank
    private String username;
    @NotNull
    private int points;

    public ChallengeUser convert(){
        ChallengeUser challengeUser = new ChallengeUser();
        challengeUser.setUsername(this.username);
        challengeUser.setPoints(this.points);
        return challengeUser;
    }
}
