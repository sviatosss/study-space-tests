package com.sviatosss.testingapp.payload.response;

import com.sviatosss.testingapp.model.TestWrap;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class TestWrapsResponse {
    List<TestWrap> results;
}
