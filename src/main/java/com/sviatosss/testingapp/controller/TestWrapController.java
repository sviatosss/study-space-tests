package com.sviatosss.testingapp.controller;

import com.sviatosss.testingapp.event.OnNewTestWrapEvent;
import com.sviatosss.testingapp.exceptions.IDException;
import com.sviatosss.testingapp.model.Tag;
import com.sviatosss.testingapp.model.Test;
import com.sviatosss.testingapp.model.TestWrap;
import com.sviatosss.testingapp.model.enums.ESubject;
import com.sviatosss.testingapp.payload.response.TestWrapsResponse;
import com.sviatosss.testingapp.repository.TestWrapRepository;
import com.sviatosss.testingapp.utils.validator.Validator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/test-wrap")
public class TestWrapController {

    @Autowired
    private ApplicationEventPublisher eventPublisher;

    @Autowired
    private TestWrapRepository testWrapRepository;

    @PostMapping("")
    public ResponseEntity<?> uploadTestWrap(@RequestBody TestWrap testWrap, BindingResult bindingResult){
        if (bindingResult.hasErrors()){
            return new Validator().getErrors(bindingResult);
        }

        List<Tag> tags = new ArrayList<>();
        List<Tag> tagsNull = new ArrayList<>();
        for (Tag tag:testWrap.getTags()) {
            if (tag.getId() == null) tagsNull.add(tag);
            else tags.add(tag);
        }
        testWrap.setTags(tags);

        TestWrap testWrapNew = testWrapRepository.save(testWrap);

        if (tagsNull.size() > 0) eventPublisher.publishEvent(new OnNewTestWrapEvent(testWrapNew.getId(), tagsNull));

        System.out.println("return");
        return new ResponseEntity<TestWrap>(testWrapNew, HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> addTestToTestWrap(@RequestBody Test test, BindingResult bindingResult, @PathVariable String id){
        if (bindingResult.hasErrors()){
            return new Validator().getErrors(bindingResult);
        }
        return new ResponseEntity<TestWrap>(testWrapRepository.addTest(id, test), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getTestWrap(@PathVariable String id){
        TestWrap testWrap = testWrapRepository.getTestWrapById(id);
        if (testWrap != null) return new ResponseEntity<>(testWrap, HttpStatus.OK);
        else return new ResponseEntity<>(new IDException("testWrap with id - " + id + " not found !"), HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/{id}")
    public void deleteTestWrap(@PathVariable String id){
        testWrapRepository.deleteById(id);
    }

    @GetMapping("/subject/{eSubject}")
    public ResponseEntity<?> getListTestWrapsBySubject(@PathVariable ESubject eSubject){
        List<TestWrap> testWrapList = testWrapRepository.getTestWrapByESubject(eSubject);
        if (testWrapList.size() > 0) return new ResponseEntity<>(testWrapList, HttpStatus.OK);
        else return new ResponseEntity<>(new IDException("testWraps with subject - " + eSubject + " not found !"), HttpStatus.NOT_FOUND);
    }

    @GetMapping("/user-id/{userId}")
    public ResponseEntity<?> getListTestWrapsByUserId(@PathVariable Long userId){
        List<TestWrap> testWrapList = testWrapRepository.getTestWrapByUserId(userId);
        if (testWrapList.size() > 0) return new ResponseEntity<>(testWrapList, HttpStatus.OK);
        else return new ResponseEntity<>(new IDException("testWraps with subject - " + userId + " not found !"), HttpStatus.NOT_FOUND);
    }

    @CrossOrigin
    @GetMapping("")
    public ResponseEntity<?> getAllTestWraps(){
        TestWrapsResponse response = new TestWrapsResponse(testWrapRepository.findAll());
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
