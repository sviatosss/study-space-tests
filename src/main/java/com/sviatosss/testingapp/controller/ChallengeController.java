package com.sviatosss.testingapp.controller;

import com.sviatosss.testingapp.event.OnNewTestWrapEvent;
import com.sviatosss.testingapp.exceptions.IDException;
import com.sviatosss.testingapp.model.Tag;
import com.sviatosss.testingapp.model.Test;
import com.sviatosss.testingapp.model.TestWrap;
import com.sviatosss.testingapp.model.enums.ESubject;
import com.sviatosss.testingapp.model.сhallenge.Challenge;
import com.sviatosss.testingapp.model.сhallenge.ChallengeUser;
import com.sviatosss.testingapp.payload.request.ChallengeAddPointRequest;
import com.sviatosss.testingapp.payload.request.ChallengeJoinRequest;
import com.sviatosss.testingapp.repository.ChallengeRepository;
import com.sviatosss.testingapp.repository.TestWrapRepository;
import com.sviatosss.testingapp.utils.validator.Validator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@Controller
public class ChallengeController {
    @Autowired
    private SimpMessagingTemplate messagingTemplate;

    @Autowired
    private ChallengeRepository challengeRepository;

    @PostMapping("/challenge/create")
    public ResponseEntity<?> uploadTestWrap(@RequestBody Challenge challenge, BindingResult bindingResult){
        if (bindingResult.hasErrors()){
            return new Validator().getErrors(bindingResult);
        }
        Challenge challengeCreated = challengeRepository.createChallenge(challenge);
        return new ResponseEntity<>(challengeCreated, HttpStatus.CREATED);
    }

    @GetMapping("/challenge/{challengeId}")
    public ResponseEntity<?> getChallengeById(@PathVariable String challengeId){
        Challenge challenge = challengeRepository.getChallengeById(challengeId);
        if (challenge == null) return new ResponseEntity<>(
                new IDException("not found challenge with id - " + challengeId), HttpStatus.NOT_FOUND);
        else return new ResponseEntity<>(challenge, HttpStatus.OK);
    }

    @MessageMapping("/challenge/join")
    public void joinToChallenge(@Payload ChallengeJoinRequest challengeJoinRequest) {
        Challenge challenge = challengeRepository.joinToChallenge(challengeJoinRequest.getChallengeId(), challengeJoinRequest.convert());

        if (challenge == null) return;
        else messagingTemplate.convertAndSendToUser("challenge/", challenge.getId(), challenge);
    }

    @MessageMapping("/challenge/start")
    public void startChallenge(@Payload String challengeId) {
        Challenge challenge = challengeRepository.startChallenge(challengeId);

        if (challenge == null) return;
        else messagingTemplate.convertAndSendToUser("challenge/", challenge.getId(), challenge);
    }


    @MessageMapping("/challenge/add-point")
    public void addPoint(@Payload ChallengeAddPointRequest challengeAddPointRequest) {
        Challenge challenge = challengeRepository.addPoint(challengeAddPointRequest.getChallengeId(), challengeAddPointRequest.convert());

        if (challenge == null) return;
        else messagingTemplate.convertAndSendToUser("challenge/", challenge.getId(), challenge);
    }

}
