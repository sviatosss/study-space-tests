package com.sviatosss.testingapp.controller;

import com.sviatosss.testingapp.event.OnNewTestWrapEvent;
import com.sviatosss.testingapp.exceptions.IDException;
import com.sviatosss.testingapp.model.Tag;
import com.sviatosss.testingapp.model.Test;
import com.sviatosss.testingapp.model.TestWrap;
import com.sviatosss.testingapp.model.enums.ESubject;
import com.sviatosss.testingapp.repository.TagRepository;
import com.sviatosss.testingapp.repository.TestWrapRepository;
import com.sviatosss.testingapp.utils.validator.Validator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/tag")
public class TagController {

    @Autowired
    private TagRepository tagRepository;

    @GetMapping("{subject}/{chars}")
    public ResponseEntity<?> getTagsBySubjectAndCharsOfTitle(@PathVariable String chars, @PathVariable ESubject subject){
        List<Tag> tags = tagRepository.getTagsBySubjectAndCharsOfTitle(subject, chars);
        if (tags.size() > 0) return new ResponseEntity<>(tags, HttpStatus.OK);
        else return new ResponseEntity<>(new IDException("tag with subject - '" + subject + "' and  chars - '" + chars + "' not found !"), HttpStatus.NOT_FOUND);
    }
}
