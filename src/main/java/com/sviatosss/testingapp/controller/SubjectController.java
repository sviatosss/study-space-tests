package com.sviatosss.testingapp.controller;

import com.sviatosss.testingapp.exceptions.IDException;
import com.sviatosss.testingapp.model.Subject;
import com.sviatosss.testingapp.model.Tag;
import com.sviatosss.testingapp.model.enums.ESubject;
import com.sviatosss.testingapp.repository.SubjectRepository;
import com.sviatosss.testingapp.repository.TagRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/subject")
public class SubjectController {

    @Autowired
    private SubjectRepository subjectRepository;

    @GetMapping("")
    public ResponseEntity<?> getAllSubject(){
        List<Subject> subjects = subjectRepository.findAll();
        if (subjects.size() > 0) return new ResponseEntity<>(subjects, HttpStatus.OK);
        else return new ResponseEntity<>(new IDException(" subject not found !"), HttpStatus.NOT_FOUND);
    }

    @PostMapping("")
    public ResponseEntity<?> addSubject(@RequestBody Subject subject){
        return new ResponseEntity<>(subjectRepository.save(subject), HttpStatus.CREATED);
    }
}
