package com.sviatosss.testingapp.model.test_type;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;


@Getter
@Setter
@Document
public class TestMultiAnswer extends TestContent {
    private List<String> options;
    private List<String> answers;
}