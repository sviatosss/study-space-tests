package com.sviatosss.testingapp.model.test_type;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;


@Getter
@Setter
@Document
public class TestMultiImagesAnswer extends TestContent {
    private List<String> images;
    private List<String> answers;
}