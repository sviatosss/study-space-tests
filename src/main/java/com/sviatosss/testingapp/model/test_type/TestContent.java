package com.sviatosss.testingapp.model.test_type;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;
import com.sviatosss.testingapp.model.enums.ETestType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@AllArgsConstructor
@JsonTypeInfo(use = Id.NAME, include = As.PROPERTY, property = "type")
@JsonSubTypes(value = {
        @JsonSubTypes.Type(value = TestSingleAnswer.class, name = ETestType.SINGLE_ANSWER),
        @JsonSubTypes.Type(value = TestSingleImagesAnswer.class, name = ETestType.SINGLE_IMAGES_ANSWER),
        @JsonSubTypes.Type(value = TestMultiAnswer.class, name = ETestType.MULTI_ANSWER),
        @JsonSubTypes.Type(value = TestMultiImagesAnswer.class, name = ETestType.MULTI_IMAGES_ANSWER),
        @JsonSubTypes.Type(value = TestBoolean.class, name = ETestType.BOOLEAN),
        @JsonSubTypes.Type(value = TestAccordance.class, name = ETestType.ACCORDANCE)
})
public class TestContent {
}