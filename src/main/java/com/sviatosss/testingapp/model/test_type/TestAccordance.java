package com.sviatosss.testingapp.model.test_type;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Map;


@Getter
@Setter
@Document
public class TestAccordance extends TestContent {
    private Map<String, String> accordancies;
}