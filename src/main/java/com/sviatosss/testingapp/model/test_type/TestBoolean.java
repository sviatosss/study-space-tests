package com.sviatosss.testingapp.model.test_type;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;


@Getter
@Setter
@Document
public class TestBoolean extends TestContent {
    private String question;
    private boolean answer;
}