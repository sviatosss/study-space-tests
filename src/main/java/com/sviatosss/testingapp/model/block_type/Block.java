package com.sviatosss.testingapp.model.block_type;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;
import com.sviatosss.testingapp.model.enums.EBlockType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@AllArgsConstructor
@JsonTypeInfo(use = Id.NAME, include = As.PROPERTY, property = "type")
@JsonSubTypes(value = {
        @JsonSubTypes.Type(value = BlockText.class, name = EBlockType.TEXT),
        @JsonSubTypes.Type(value = BlockImage.class, name = EBlockType.IMAGE),
        @JsonSubTypes.Type(value = BlockVideo.class, name = EBlockType.VIDEO),
        @JsonSubTypes.Type(value = BlockAudio.class, name = EBlockType.AUDIO)
})
public class Block {
}