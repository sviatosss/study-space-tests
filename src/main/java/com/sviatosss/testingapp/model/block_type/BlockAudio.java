package com.sviatosss.testingapp.model.block_type;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;


@Getter
@Setter
@Document
public class BlockAudio extends Block {
    private String audioUrl;
}