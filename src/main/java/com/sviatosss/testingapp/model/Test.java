package com.sviatosss.testingapp.model;

import com.sviatosss.testingapp.model.block_type.Block;
import com.sviatosss.testingapp.model.test_type.TestContent;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Test {
    @Transient
    public static final String SEQUENCE__NAME = "test__sequence";

    @Id
    private long id;

    private Block title;
    private TestContent content;
    private Block explanation;
    private int time;
}