package com.sviatosss.testingapp.model;

import com.sviatosss.testingapp.model.block_type.Block;
import com.sviatosss.testingapp.model.enums.ESubject;
import com.sviatosss.testingapp.model.test_type.TestContent;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Document("test_wraps")
public class TestWrap {
    @Id
    private String id;
    private Long userId;
    private String title;
    private ESubject subject;

    @DBRef
    List<Tag> tags;

    List<Test> tests;

    public void addTest(Test test){
        this.tests.add(test);
    }

    public void addTags(List<Tag> tags){
        this.tags.addAll(tags);
    }
}