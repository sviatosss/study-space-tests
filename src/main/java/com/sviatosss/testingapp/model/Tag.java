package com.sviatosss.testingapp.model;

import com.sviatosss.testingapp.model.enums.ESubject;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Document("tags")
public class Tag {
    @Id
    private String id;
    private ESubject subject;

    @Indexed
    private String title;
}