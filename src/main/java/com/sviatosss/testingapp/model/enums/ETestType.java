package com.sviatosss.testingapp.model.enums;

public class ETestType {
  public static final String SINGLE_ANSWER = "SINGLE_ANSWER";
  public static final String SINGLE_IMAGES_ANSWER = "SINGLE_IMAGES_ANSWER";
  public static final String MULTI_ANSWER = "MULTI_ANSWER";
  public static final String MULTI_IMAGES_ANSWER = "MULTI_IMAGES_ANSWER";
  public static final String BOOLEAN = "BOOLEAN";
  public static final String ACCORDANCE = "ACCORDANCE";
}