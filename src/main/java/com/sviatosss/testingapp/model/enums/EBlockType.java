package com.sviatosss.testingapp.model.enums;

public class EBlockType {
  public static final String TEXT = "TEXT";
  public static final String IMAGE = "IMAGE";
  public static final String VIDEO = "VIDEO";
  public static final String AUDIO = "AUDIO";
}