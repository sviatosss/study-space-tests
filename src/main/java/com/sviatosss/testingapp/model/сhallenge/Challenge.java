package com.sviatosss.testingapp.model.сhallenge;

import com.sviatosss.testingapp.model.сhallenge.ChallengeUser;
import com.sviatosss.testingapp.model.сhallenge.EStatusChallenge;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Document("challenges")
public class Challenge {
    @Id
    private String id;

    private EStatusChallenge status = EStatusChallenge.CREATED_WAITING;
    private Long creatorId;
    private String creatorUsername;
    private String testId;
    private String testTitle;
    private List<ChallengeUser> users;
    private int maxUsers;

    public List<ChallengeUser> addChallengeUser(ChallengeUser challengeUser){
        this.users.add(challengeUser);
        return this.users;
    }
}
