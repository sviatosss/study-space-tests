package com.sviatosss.testingapp.model.сhallenge;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ChallengeUser {
    private Long id;
    private ETypeChallengeUser type;
    private String username;
    private int points = 0;
}
