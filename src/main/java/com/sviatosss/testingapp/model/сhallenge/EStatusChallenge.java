package com.sviatosss.testingapp.model.сhallenge;

public enum  EStatusChallenge {
    CREATED_WAITING,
    CREATED_READY,
    STARTED,
    DONE
}
