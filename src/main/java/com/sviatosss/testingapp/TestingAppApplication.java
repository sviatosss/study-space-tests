package com.sviatosss.testingapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
//@EnableDiscoveryClient
public class TestingAppApplication {
    public static void main(String[] args) {
        SpringApplication.run(TestingAppApplication.class, args);
    }
}